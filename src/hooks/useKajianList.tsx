import { useReducer, useState } from "react";
import axios from "axios";

interface State {
  data: Kajian[] | null;
  error: string | null;
  loading: boolean;
}

interface Kajian {
  id: string;
  title: string;
  description: string;
  ustadz: string;
  thumbnailUrl: string;
  videoUrl: string;
  duration: number;
  category: string;
}

const initialState: State = {
  data: null,
  error: null,
  loading: false,
};

enum ActionType {
  LOADING,
  SUCCESS,
  FAILED,
}

type Action =
  | { type: ActionType.LOADING }
  | { type: ActionType.SUCCESS; payload: Kajian[] }
  | { type: ActionType.FAILED; payload: string };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionType.LOADING:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ActionType.FAILED:
      return {
        loading: false,
        error: action.payload,
        data: null,
      };
    case ActionType.SUCCESS:
      return {
        loading: false,
        error: null,
        data: action.payload,
      };
    default:
      return initialState;
  }
};

const useKajianList = (offset: number) => {
  const [{ data, loading, error }, dispatch] = useReducer(
    reducer,
    initialState,
  );

  const [count, setCount] = useState<number | null>();

  const fetchKajianList = async () => {
    if (data && count && data.length >= count) return;
    dispatch({ type: ActionType.LOADING });
    try {
      const response = await axios.get(
        "http://localhost:8080/kajian/list?offset=" + offset,
      );
      const kajianData = data
        ? [...data, ...response.data.kajians]
        : response.data.kajians;
      setCount(response.data.count);
      dispatch({ type: ActionType.SUCCESS, payload: kajianData });
    } catch (error) {
      dispatch({ type: ActionType.FAILED, payload: "Something went wrong!" });
    }
  };

  return { data, loading, error, fetchKajianList };
};

export default useKajianList;

// LOADING
// ERROR
// SUCCESS
