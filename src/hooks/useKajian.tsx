import { useReducer } from "react";
import axios from "axios";

interface State {
  data: Kajian | null;
  error: string | null;
  loading: boolean;
}

interface Kajian {
  id: string;
  title: string;
  description: string;
  ustadz: string;
  thumbnailUrl: string;
  videoUrl: string;
  duration: number;
  category: string;
}

const initialState: State = {
  data: null,
  error: null,
  loading: false,
};

enum ActionType {
  LOADING,
  SUCCESS,
  FAILED,
}

type Action =
  | { type: ActionType.LOADING }
  | { type: ActionType.SUCCESS; payload: Kajian }
  | { type: ActionType.FAILED; payload: string };

const reducer = (_: State, action: Action): State => {
  switch (action.type) {
    case ActionType.LOADING:
      return {
        loading: true,
        error: null,
        data: null,
      };
    case ActionType.FAILED:
      return {
        loading: false,
        error: action.payload,
        data: null,
      };
    case ActionType.SUCCESS:
      return {
        loading: false,
        error: null,
        data: action.payload,
      };
    default:
      return initialState;
  }
};

const useKajian = () => {
  const [{ data, loading, error }, dispatch] = useReducer(
    reducer,
    initialState,
  );

  const fetchKajian = async (id: string) => {
    dispatch({ type: ActionType.LOADING });
    try {
      const response = await axios.get("http://localhost:8080/kajian/" + id);
      dispatch({ type: ActionType.SUCCESS, payload: response.data });
    } catch (error) {
      dispatch({ type: ActionType.FAILED, payload: "Something went wrong!" });
    }
  };

  return { data, loading, error, fetchKajian };
};

export default useKajian;

// LOADING
// ERROR
// SUCCESS
