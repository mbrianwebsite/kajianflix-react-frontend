import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";

import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./app/store";
import HomePage from "./pages/HomePage";
import SignInPage from "./pages/SignInPage";
import SignUpPage from "./pages/SignUpPage";
import BrowsePage from "./pages/BrowsePage";
import WatchPage from "./pages/WatchPage";
import PlansPage from "./pages/PlansPage";
import PrivateRoutes from "./utils/PrivateRoutes";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<App />}>
      <Route path="/" element={<HomePage />}></Route>
      <Route path="/masuk" element={<SignInPage />}></Route>
      <Route path="/daftar" element={<SignUpPage />}></Route>
      <Route path="/harga" element={<PlansPage />}></Route>
      <Route path="/kajian" element={<PrivateRoutes />}>
        <Route path="/kajian" element={<BrowsePage />}></Route>
        <Route path="/kajian/:id" element={<WatchPage />}></Route>
      </Route>
    </Route>,
  ),
);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>,
);
