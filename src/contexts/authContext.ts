import { UseFormRegister, FieldErrors } from "react-hook-form";
import { createContext } from "react";

export type Inputs = {
  email: string;
  username: string;
  password: string;
  search: string;
};

interface AuthFormContextType {
  register: UseFormRegister<Inputs> | null;
  errors: FieldErrors<Inputs>;
}

export const AuthFormContext = createContext<AuthFormContextType>({
  register: null,
  errors: {},
});
