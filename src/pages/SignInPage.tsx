import NavBar from "../components/NavBar";
import SignInCard from "../components/SignInCard";

export default function SignInPage() {
  return (
    <>
      <NavBar />
      <div className="flex h-screen w-screen items-center justify-center bg-black bg-opacity-80">
        <SignInCard />
      </div>
    </>
  );
}
