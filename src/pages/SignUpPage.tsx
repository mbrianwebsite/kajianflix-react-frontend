import NavBar from "../components/NavBar";
import SignUpCard from "../components/SignUpCard";

export default function SignUpPage() {
  return (
    <>
      <NavBar />
      <div className="flex h-screen w-screen items-center justify-center bg-black bg-opacity-80">
        <SignUpCard />
      </div>
    </>
  );
}
