import HomeBanner from "../components/HomeBanner";
import NavBar from "../components/NavBar";

export default function HomePage() {
  return (
    <>
      <NavBar />
      <HomeBanner />
    </>
  );
}
