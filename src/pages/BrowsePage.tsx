import { useCallback, useEffect, useRef, useState } from "react";
import { IconContext } from "react-icons";
// import imgPlaceholder from "../assets/imgPlaceholder.jpg";
import kajianFlixBanner from "../assets/KAJIANFLIX-BANNER.webp";
import NavBar from "../components/NavBar";
import PrimaryButton from "../components/PrimaryButton";
import { FaPlayCircle, FaStopwatch } from "react-icons/fa";
import Input from "../components/Input";
import useKajianList from "../hooks/useKajianList";
import { useNavigate } from "react-router-dom";
import LoadingGrid from "../components/LoadingGrid";

import { useSelector } from "react-redux";
import { RootState } from "../app/store";

interface Kajian {
  id: string;
  title: string;
  description: string;
  ustadz: string;
  thumbnailUrl: string;
  videoUrl: string;
  duration: number;
  category: string;
}

const card = (
  { id, thumbnailUrl, title, ustadz, category }: Kajian,
  index: number,
  handleWatch: (id: number) => void,
  lastElementRef: ((node: HTMLDivElement) => void) | null,
) => {
  return (
    <div
      ref={lastElementRef}
      className=" group relative aspect-video"
      key={index}
    >
      <div className="onHoverAnimation flex flex-col group-hover:absolute group-hover:z-40 group-hover:scale-110 group-hover:shadow-lg">
        <img
          className="aspect-video  rounded-md bg-gray-900 object-cover group-hover:rounded-b-none "
          src={thumbnailUrl}
          alt=""
        />
        <div className="hidden flex-col gap-3 rounded-b-md bg-stone-900 p-4 text-white group-hover:flex">
          <div className="flex flex-row items-center justify-between">
            <IconContext.Provider value={{ className: "fill-white w-8 h-8" }}>
              <div className="" onClick={() => handleWatch(parseInt(id))}>
                <FaPlayCircle />
              </div>
            </IconContext.Provider>
            <IconContext.Provider
              value={{
                className: "fill-white w-8 h-8",
              }}
            >
              <div className="">
                <FaStopwatch />
              </div>
            </IconContext.Provider>
          </div>
          <div className="flex flex-col gap-4">
            <div className="flex flex-col  gap-1">
              <div className="text-base font-bold leading-none md:text-lg">
                {title}
              </div>
              <div className="text-sm text-slate-200 md:text-base">
                {ustadz}
              </div>
            </div>
            <div className=":text-sm my-1 w-fit rounded-md bg-slate-100 px-2 py-1 text-xs font-semibold capitalize text-slate-900">
              {category}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const grid = (
  datas: Kajian[],
  handleWatch: (id: number) => void,
  lastElementRef: (node: HTMLDivElement) => void,
) => {
  return (
    <div className="flex flex-col gap-4">
      <div className="z-0 text-2xl font-bold text-gray-900">Kajian Terbaru</div>
      <div className="grid grid-cols-2 items-center gap-4 overflow-visible md:grid-cols-3 lg:grid-cols-4">
        {datas?.map((data, index) =>
          card(
            data,
            index,
            handleWatch,
            datas.length === index + 1 ? lastElementRef : null,
          ),
        )}
      </div>
    </div>
  );
};

export default function BrowsePage() {
  const [offset, setOffset] = useState(0);
  const { data, loading, error, fetchKajianList } = useKajianList(offset);

  const observer = useRef<null | IntersectionObserver>(null);

  const { user, isLoading } = useSelector(
    (state: RootState) => state.user.value,
  );

  console.log({ isLoading, user });

  const lastElementRef = useCallback(
    (node: HTMLDivElement) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          setOffset(offset + 4);
          console.log("Intersecting");
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading],
  );
  const navigate = useNavigate();

  const handleWatch = (id: number) => {
    navigate("/kajian/" + id);
  };
  useEffect(() => {
    fetchKajianList();
  }, [offset]);
  return (
    <>
      <NavBar />
      <img
        className="h-screen w-screen object-cover brightness-[0.2]"
        src={kajianFlixBanner}
        alt=""
      />
      <div className="absolute top-0 z-10 flex h-screen w-screen items-center justify-center px-4">
        <div className="flex flex-col gap-6 md:items-center">
          <div className="flex flex-col gap-2 md:items-center">
            <div className="text-4xl font-bold text-white">
              Jelajahi Kajian, dan Ambil Faidah!
            </div>
            <div className="text-xl font-normal text-white">
              Seluruh video kami ambil dari Youtube dan telah kami kurasi untuk
              ditampilkan
            </div>
          </div>
          <div className="flex flex-col gap-4 md:flex-row">
            <Input name="search" type="text" placeholder="Kata Kunci" />
            <PrimaryButton title="Jelajahi" />
          </div>
        </div>
      </div>
      <div className="mx-auto flex w-full max-w-7xl flex-col gap-8 px-6 py-8">
        {data && grid(data, handleWatch, lastElementRef)}
        {loading && <LoadingGrid />}
        {error && error}
      </div>
    </>
  );
}
