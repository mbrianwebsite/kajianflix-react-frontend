import { IconContext } from "react-icons";
import NavBar from "../components/NavBar";
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa";
import PrimaryButton from "../components/PrimaryButton";

export default function PlansPage() {
  return (
    <>
      <NavBar />
      <div className="flex h-screen w-screen items-center justify-center bg-black bg-opacity-90">
        <div className="flex flex-col gap-4">
          <div className="flex flex-col gap-4 md:flex-row">
            <div className="flex flex-col">
              <div className="flex flex-col gap-1 rounded-t-md bg-gradient-to-r from-blue-600 to-blue-400 p-5">
                <div className="text-2xl font-bold text-white">Biasa</div>
                <div className="font-medium text-white">
                  Rp. 0 <span className="font-semibold">(Gratis)</span>
                </div>
              </div>
              <div className="flex flex-col gap-4 rounded-b-md bg-white p-5">
                <div className="flex flex-row items-center gap-2  bg-white">
                  <IconContext.Provider
                    value={{ className: "fill-emerald-400", size: "24px" }}
                  >
                    <FaCheckCircle />
                  </IconContext.Provider>
                  <div>Nonton Semua Video</div>
                </div>
                <div className="flex flex-row items-center gap-2  bg-white">
                  <IconContext.Provider
                    value={{ className: "fill-red-400", size: "24px" }}
                  >
                    <FaTimesCircle />
                  </IconContext.Provider>
                  <div>Tidak Donasi</div>
                </div>
              </div>
            </div>
            <div className="flex flex-col">
              <div className="flex flex-col gap-1 rounded-t-md bg-gradient-to-r from-brand to-red-400 p-5">
                <div className="text-2xl font-bold text-white">Donatur</div>
                <div className="font-medium text-white">
                  Rp. 10.000 <span className="font-semibold">/Bulan</span>
                </div>
              </div>
              <div className="flex flex-col gap-4 rounded-b-md bg-white p-5">
                <div className="flex flex-row items-center gap-2  bg-white">
                  <IconContext.Provider
                    value={{ className: "fill-emerald-400", size: "24px" }}
                  >
                    <FaCheckCircle />
                  </IconContext.Provider>
                  <div>Nonton Semua Video</div>
                </div>
                <div className="flex flex-row items-center gap-2  bg-white">
                  <IconContext.Provider
                    value={{ className: "fill-emerald-400", size: "24px" }}
                  >
                    <FaCheckCircle />
                  </IconContext.Provider>
                  <div>Ikut Donasi</div>
                </div>
              </div>
            </div>
          </div>
          <PrimaryButton title="Daftar Donatur" />
        </div>
      </div>
    </>
  );
}
