import NavBar from "../components/NavBar";
import PrimaryButton from "../components/PrimaryButton";
import { useParams } from "react-router-dom";
import useKajian from "../hooks/useKajian";
import { useEffect } from "react";

export default function WatchPage() {
  const params = useParams() as { id: string };

  const { data, loading, error, fetchKajian } = useKajian();

  useEffect(() => {
    fetchKajian(params.id);
  }, []);

  return (
    <>
      <NavBar />
      {data && (
        <div className="relative">
          <img
            className="h-screen w-screen object-cover brightness-[0.2]"
            src={data.thumbnailUrl}
            alt=""
          />
          <div className="absolute top-0 z-10 flex h-screen w-screen items-start px-4 lg:items-center">
            <div className="mx-auto flex w-full max-w-7xl flex-col-reverse items-center gap-8 pt-20 md:pt-32 lg:flex-row lg:items-start lg:pt-0 ">
              <div className="flex flex-col items-center gap-4 lg:w-1/2 lg:items-start ">
                <div className="flex flex-col gap-1">
                  <div className="text-center text-4xl font-bold text-white lg:text-start">
                    {data.title}
                  </div>
                  <div className="text-center text-2xl font-normal text-white lg:text-start">
                    {data.ustadz}
                  </div>
                </div>
                <div className="line-clamp-3 text-justify text-xl font-normal text-gray-300 lg:line-clamp-5 lg:text-start">
                  {data.description}
                </div>
                <div>
                  <PrimaryButton title="Tonton Nanti" />
                </div>
              </div>
              <div className="flex aspect-video w-full lg:w-1/2">
                <iframe
                  className="rounded-2xl"
                  width="100%"
                  height="100%"
                  src={data.videoUrl}
                  title="YouTube video player"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      )}
      {error && error}
      {loading && "Loading..."}
    </>
  );
}
