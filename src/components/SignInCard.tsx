import Input from "./Input";
import PrimaryButton from "./PrimaryButton";
import { Link } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import useAuth from "../hooks/useAuth";

import { Inputs, AuthFormContext } from "../contexts/authContext";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function SignInCard() {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<Inputs>();

  const { signIn } = useAuth();

  const [authError, setAuthError] = useState("");

  const onSubmit: SubmitHandler<Inputs> = async ({ email, password }) => {
    try {
      await signIn({
        email,
        password,
      });
      setAuthError("");
      navigate("/kajian");
    } catch (error: any) {
      setAuthError(error.response.data.errors[0].msg);
    }
  };
  return (
    <>
      <div className="m-4 flex w-full max-w-md flex-col items-center gap-10 rounded-xl bg-black bg-opacity-70 p-6 md:p-16">
        <div className="w-full text-left text-4xl font-semibold text-white">
          Masuk
        </div>
        <AuthFormContext.Provider
          value={{
            register,
            errors,
          }}
        >
          <form
            className="flex w-full flex-col gap-10"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="flex w-full flex-col gap-3">
              <Input name="email" placeholder="Email" type="email" />
              <Input
                name="password"
                placeholder="Password"
                type="password"
                validate={() => {
                  const password = getValues("password");
                  if (password.length < 6) {
                    return "Password harus 6 atau lebih";
                  }
                  if (!/[A-Z]/.test(password)) {
                    return "Password harus mengandung huruf kapital";
                  }
                  if (!/[a-z]/.test(password)) {
                    return "Password harus mengandung satu huruf kecil";
                  }
                  if (!/[0-9]/.test(password)) {
                    return "Password harus mengandung satu angka";
                  }
                  return true;
                }}
              />
            </div>
            <div className="flex flex-col gap-2">
              <PrimaryButton title="Masuk" />
              {authError && <p className="text-red-300">{authError}</p>}
            </div>
          </form>
        </AuthFormContext.Provider>
        <div className="w-full text-left  text-white">
          Belum punya akun KajianFlix?{" "}
          <span className="font-bold">
            <Link to="/daftar">Daftar di sini!</Link>
          </span>
        </div>
      </div>
    </>
  );
}
