interface primaryButtonProps {
  title: string;
  moreClass?: string;
  handleOnClick?: () => void;
}

export default function PrimaryButton({
  title,
  moreClass,
  handleOnClick,
}: primaryButtonProps) {
  return (
    <>
      <button
        onClick={handleOnClick}
        className={
          "rounded-md bg-brand px-5 py-2 text-lg font-semibold text-white hover:brightness-150 " +
          moreClass
        }
      >
        {title}
      </button>
    </>
  );
}
