import { useContext } from "react";
import { AuthFormContext, Inputs } from "../contexts/authContext";

interface inputProps {
  placeholder: string;
  type: string;
  moreClass?: string;
  name: keyof Inputs;
  validate?: (text: string) => string | true;
}

export default function Input({
  placeholder,
  type,
  moreClass,
  name,
  validate,
}: inputProps) {
  const { register, errors } = useContext(AuthFormContext);

  if (!register) return null;

  return (
    <>
      <div className="flex flex-col">
        <input
          className={"rounded-md px-5 py-2 text-lg font-semibold " + moreClass}
          type={type}
          placeholder={placeholder}
          {...register(name, {
            required: true,
            validate,
          })}
        />
        {errors[name]?.type === "required" && (
          <p className="text-red-300">Wajib di isi.</p>
        )}
        {errors[name]?.type === "validate" && (
          <p className="text-red-300">{errors[name]?.message}</p>
        )}
      </div>
    </>
  );
}
