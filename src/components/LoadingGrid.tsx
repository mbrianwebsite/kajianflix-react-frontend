export default function LoadingGrid() {
  return (
    <div className="flex flex-col gap-4">
      <div className="grid grid-cols-2 items-center gap-4 overflow-visible md:grid-cols-3 lg:grid-cols-4">
        {[1, 2, 3, 4]?.map((num) => (
          <div
            key={num}
            className="aspect-video  rounded-md bg-gray-900 object-cover group-hover:rounded-b-none "
          ></div>
        ))}
      </div>
    </div>
  );
}
