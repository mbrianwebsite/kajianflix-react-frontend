import { FaBars } from "react-icons/fa";
import kajianFlix from "../assets/KAJIANFLIX.png";
import { IconContext } from "react-icons";
import PrimaryButton from "./PrimaryButton";
import { Link, useNavigate } from "react-router-dom";

export default function NavBar() {
  const navigate = useNavigate();

  const handleToSignIn = () => {
    navigate("/masuk");
  };

  const handleToHome = () => {
    navigate("/");
  };
  return (
    <>
      <nav className="fixed top-0 z-20 mx-auto flex w-screen max-w-7xl flex-row items-center justify-between gap-4 rounded-b-md bg-black bg-opacity-70 p-4 md:justify-start md:gap-12 md:p-6">
        <div className="flex flex-row items-center gap-4">
          <IconContext.Provider value={{ className: "fill-white" }}>
            <div className="rounded-sm p-1 ring-1 ring-white md:hidden">
              <FaBars />
            </div>
          </IconContext.Provider>
          <img
            onClick={handleToHome}
            className="max-h-6 md:max-h-10"
            src={kajianFlix}
            alt="Kajian Flix"
          />
        </div>
        <div className="hidden w-full flex-row items-center gap-4 text-lg font-medium text-white  md:flex">
          <Link to="/kajian">Jelajah</Link>
          <Link to="/harga">Harga</Link>
          <a>Tentang</a>
        </div>
        <div className="flex flex-row items-center gap-4">
          <PrimaryButton
            title="Masuk"
            moreClass="hidden md:inline"
            handleOnClick={handleToSignIn}
          />
        </div>
      </nav>
    </>
  );
}
