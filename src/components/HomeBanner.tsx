import kajianFlixBanner from "../assets/KAJIANFLIX-BANNER.webp";
import PrimaryButton from "./PrimaryButton";
import { useNavigate } from "react-router-dom";

export default function HomeBanner() {
  const navigate = useNavigate();

  const handleToSignUp = () => {
    navigate("/daftar");
  };
  return (
    <>
      <div className="relative">
        <img
          className="h-screen w-screen object-cover brightness-[0.2]"
          src={kajianFlixBanner}
          alt=""
        />
        <div className="absolute top-0 z-10 flex h-screen w-screen items-center justify-center px-4">
          <div className="flex flex-col gap-6 md:items-center">
            <div className="flex flex-col gap-2 md:items-center">
              <div className="text-4xl font-bold text-white">
                Ngaji Tanpa Distraksi
              </div>
              <div className="text-xl font-normal text-white">
                Ambil setiap faidah kajian tanpa gangguan video lain
              </div>
            </div>
            <PrimaryButton title="Daftar" handleOnClick={handleToSignUp} />
          </div>
        </div>
      </div>
    </>
  );
}
